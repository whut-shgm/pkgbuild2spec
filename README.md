# PKGBUILD2SPEC

## 概述
* PKGBUILD文件自动转换成SPEC文件的脚本，可以实现PKGBUILD到SPEC的编译打包转换

## 文件
1. [pkgbuild2spec.sh](./pkgbuild2spec.sh)
2. [PKGBUILD流程和示例说明](./PKGBUILD流程和示例说明.md)
3. [PKGBUILD转SPEC的示例](./PKGBUILD转SPEC的示例.md)

