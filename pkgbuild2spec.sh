#!/bin/bash

pkgs_path=$1
now_path=$pwd
pkg_file_name=PKGBUILD

pkgs_list=$(find $pkg_path -type f -name $pkg_file_name)

transform()
{
	for pkgfile in $pkgs_list
	do
		pkg_path=${pkgfile%/*}
		echo $pkg_path
		cd $pkg_path
		
		#test PKGBUILD build use pacmake
		makepkg -f $pkg_file_name
		if [ $? -ne 0 ];then
			echo -e "$pkgfile build failed !!! \n"
			continue
		else
			echo -e "$pkgfile build successed !!! \n"
		fi
			
		#achieve Mapping of variables in PKGBUILD to keywords in SPEC
		variable2keywords
		
		
		#add dependency package depository for SPEC
		spec_file=$Name-$Version.spec
		
		echo -e "%description" >> $spec_file
		
		#get build dependency of transform SPEC 
		buildrequires=$(cat ${spec_file} | grep -E "^BuildRequires:" |  awk -F ":" '{print $NF}')
		
		echo -e "BuildRequires: ${buildrequires}\n"
	
		#if need dependency,Extract dependency package name
		if [ "$buildrequires" != "" ];then
			dependency=$(echo $buildrequires | sed 's/,/ /g')
			
			echo -e "build $spec_file need dependency:${dependency}\n"
			
			#file used to save dependency installation commands
			dep_file=dep.tmp
			rm -rf $dep_file
			#insert dependency installation commands into SPEC
			echo -e "%pre" >> $spec_file
			
			for dep in $dependency
			do
				install_dependency_from_repository $dep
				cat $dep_file >> $spec_file
				rm -rf $dep_file
			done
			echo -e "\n\n" >> $spec_file
		fi
		
		#achieve Mapping of funtion in PKGBUILD to processing stage in SPEC 
		function_names=(build package)
		for function_name in ${function_names[@]}
		do
			function2stage "$function_name"
		done
		
		
		
		cd $now_path
	done
}

variable2keywords()
{
	# keywords will be used in SPEC 
	Name=$(cat PKGBUILD | grep "pkgname=" |  awk -F"=" '{print $2}')
	Version=$(cat PKGBUILD | grep "pkgver=" |  awk -F"=" '{print $2}')
	Release=$(cat PKGBUILD | grep "pkgrel=" |  awk -F"=" '{print $2}')
	Summary=$(cat PKGBUILD | grep "pkgdesc=" |  awk -F"=" '{print $2}' | sed 's/\"//g')
	License=$(cat PKGBUILD | grep "license=" |  awk -F"=" '{print $2}' | sed "s/'//g" | sed 's/[()]//g')
	URL=$(cat PKGBUILD | grep "url=" |  awk -F"=" '{print $2}')
	BuildArch=$(cat PKGBUILD | grep "arch=" |  awk -F"=" '{print $2}' | sed 's/[()]//g' | sed "s/'//g" )
	
	Source0=$(cat PKGBUILD | grep "source=" |  awk -F"=" '{print $2}' | sed 's/[()]//g')
	#replace $pkgname and $pkgver in source of PCKBUILD
	Source0=$(echo $Source0 | sed 's/$pkgname/%{name}/g' | sed 's/$pkgver/%{version}/g')
	
	BuildRequires=$(cat PKGBUILD | grep "makedepends=" |  awk -F"=" '{print $2}' | sed 's/[()]//g' | sed "s/'//g" )
	Requires=$(cat PKGBUILD | grep "^depends=" |  awk -F"=" '{print $2}' | sed 's/[()]//g' | sed "s/'//g" )
	Provides=$(cat PKGBUILD | grep "provides=" |  awk -F"=" '{print $2}' | sed 's/[()]//g' | sed "s/'//g" )
	
	
	keywords_list=(Name Version Release Summary License URL BuildArch Source0 BuildRequires Requires Provides)
	rm -rf $Name-$Version.spec
	for keywords_name in ${keywords_list[@]}
	do
		keywords=`eval echo '$'"$keywords_name"`
		#echo -e $keywords
		echo -e "$keywords_name:\t$keywords" >> $Name-$Version.spec
	done
	echo -e "\n\n\n" >> $Name-$Version.spec
		
}

function2stage()
{
	##Locate build() start position in PKGBUILD
	#local startline=$(cat PKGBUILD | grep -n "^build()" | awk -F ":" '{print $1}')
	#echo -e "$startline"
	#local endline=0
	##Locate build() end position in PKGBUILD
	#local rightbrackets=$(cat PKGBUILD | grep -n "}" | awk -F ":" '{print $1}')
	#for rightbracket in $rightbrackets
	#do
	#	if [ $rightbracket -ge $startline ];then
	#		endline=$rightbracket
	#		echo -e "$endline"
	#		break
	#	else
	#		continue
	#	fi
	#done
	

	#Get content between 'function(){' and '}'
	local function_name=$1
	local tempfile=${function_name}.tmp
	echo -e "$1"
	rm -rf $tempfile
	cat PKGBUILD | sed -n '/'"${1}()"'/,/}/{//!p}' >> $tempfile
	
	#cat $tempfile
	
	#erase begin spaces
	sed -i 's/^[ \t]*//g' $tempfile
	#replace $srcdir $pkgname $pkgver $pkgdir
	sed -i 's/$srcdir/%{_builddir}/g' $tempfile
	sed -i 's/$pkgname/%{name}/g' $tempfile
	sed -i 's/$pkgver/%{version}/g' $tempfile
	sed -i 's/$pkgdir/%{buildroot}/g' $tempfile
	
	#insert %function  to SPEC
	if [ $function_name = "package" ];then
		echo -e "%install" >> $Name-$Version.spec
	else
		echo -e "%${function_name}" >> $Name-$Version.spec
	fi
	cat $tempfile >> $Name-$Version.spec
	echo -e "\n\n\n" >> $Name-$Version.spec
	
	#instert %files after %install 
	if [ $function_name = "package" ];then
		echo -e "%files" >> $Name-$Version.spec	
		#save package files into rpm
		local files_tmp=files.tmp
		rm -rf $files_tmp
		
		#package files into rpm
		cat $tempfile | grep "mkdir" >> $files_tmp
		#get file path
		sed -i 's/mkdir//g' $files_tmp
		sed -i 's/-p//g' $files_tmp
		sed -i 's/%{buildroot}//g' $files_tmp
		sed -i 's/"//g' $files_tmp
		sed -i 's/^[ \t]*//g' $files_tmp
		
		#cat $files_tmp
		
		#insert %files
		cat $files_tmp >> $Name-$Version.spec
		echo -e "%license" >> $Name-$Version.spec
		echo -e "%doc" >> $Name-$Version.spec
		echo -e "\n\n" >> $Name-$Version.spec	
		
		#erase tmp file
		rm -rf $files_tmp
	fi
	
	#erase function tmp file
	rm -rf $tempfile
	cat $Name-$Version.spec
}

get_all_files()
{
	echo $1
	for file in $1/*
    do
		if [ -f $file ];then
			echo $file
			arr=(${arr[*]} $file)
		else
			get_all_files $file
		fi
    done
}

#dependency package repository,add installation commands to SPEC based on dep
install_dependency_from_repository()
{
	local dep_name=$1
	
	if [ $dep_name = "sbcl" ];then
		echo "rpm -ivh https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/c/cl-asdf-20101028-8.el7.noarch.rpm" >> $dep_file
		echo "rpm -ivh https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/c/common-lisp-controller-7.4-8.el7.noarch.rpm" >> $dep_file
		echo "rpm -ivh https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/s/sbcl-1.4.0-1.el7.x86_64.rpm" >> $dep_file
	fi
}

transform "$1"
