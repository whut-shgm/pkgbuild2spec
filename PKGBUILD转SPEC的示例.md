## PKGBUILD转SPEC的示例
* 主要描述buildapp的PKGBUILD转换成SPEC的过程

### PKGBUILD文件
	
	```
	# Maintainer:  <aaron.l.france@gmail.com>
	pkgname=buildapp
	pkgver=1.5.5
	pkgrel=2
	epoch=
	pkgdesc="A nice easy way to build Common Lisp images"
	arch=('i686' 'x86_64')
	url=""
	license=('BSD')
	depends=('sbcl')
	makedepends=('sbcl')
	provides=('buildapp')
	source=(https://github.com/xach/$pkgname/archive/release-$pkgver.tar.gz)
	md5sums=('a60d1f7c349075f358a1bf0bf9d1ba9c')
	
	build() {
	cd "$srcdir/$pkgname-release-$pkgver"
	make
	}
	
	package() {
	cd "$srcdir/$pkgname-release-$pkgver"
	mkdir -p "$pkgdir/usr/bin"
	make DESTDIR="$pkgdir/usr" install
	}
	
	```

### 转换思路

1. pkgname => Name

2. pkgver => Version

3. pkgrel => Release

4. epoch => Epoch

5. pkgdesc => Summary

6. arch => BuildArch

7. url => URL

8. license => License

9. depends => Requires ???

10. makedepends => BuildRequires

11. provides => Provides

12. source => Source

13. md5sums

14. build() => %build

15. package() => %install

16. $srcdir => %{_builddir}

17. $pkgdir => %{buildroot}

### 存在问题
1. 依赖包从PKGBUILD到SPEC，包名是否改变
2. 依赖包找不到，如何解决

### SPEC文件

	```
	%global debug_package %{nil}
	Name:                   buildapp
	Version:                1.5.5
	Release:                2%{?dist}
	Summary:                A nice easy way to build Common Lisp images
	
	License:                BSD
	URL:                    ""
	Source0:                https://github.com/xach/%{name}/archive/release-%{version}.tar.gz
	#BuildRequires:         sbcl
	Requires:               sbcl
	
	%description
	
	
	%prep
	#获取源码到%{_sourcedir}
	#wget %{source0} -P %{_sourcedir}
	
	rpm -ivh https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/c/cl-asdf-20101028-8.el7.noarch.rpm
	rpm -ivh https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/c/common-lisp-controller-7.4-8.el7.noarch.rpm
	rpm -ivh https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/s/sbcl-1.4.0-1.el7.x86_64.rpm
	
	%autosetup -n %{name}-release-%{version}
	
	
	%build
	make
	
	
	%install
	rm -rf $RPM_BUILD_ROOT
	mkdir -p "%{buildroot}/usr/bin"
	make DESTDIR="%{buildroot}/usr" install
	
	%clean
	
	%files
	/usr/bin/buildapp
	%license
	%doc
	
	%changelog
	* Wed Apr 27 2022 root
	-
	
	```
	
### spec的测试
	```
	[root@96d0a5fd266d x86_64]# yum install buildapp-1.5.5-2.x86_64.rpm
	Last metadata expiration check: 0:41:26 ago on Wed 27 Apr 2022 11:54:53 AM UTC.
	Dependencies resolved.
	===================================================================================================================================================================
	Package                               Architecture                        Version                                 Repository                                 Size
	===================================================================================================================================================================
	Installing:
	buildapp                              x86_64                              1.5.5-2                                 @commandline                              117 k
	
	Transaction Summary
	===================================================================================================================================================================
	Install  1 Package
	
	Total size: 117 k
	Installed size: 258 k
	Is this ok [y/N]: y
	Downloading Packages:
	Running transaction check
	Transaction check succeeded.
	Running transaction test
	Transaction test succeeded.
	Running transaction
	Preparing        :                                                                                                                                           1/1
	Installing       : buildapp-1.5.5-2.x86_64                                                                                                                   1/1
	Verifying        : buildapp-1.5.5-2.x86_64                                                                                                                   1/1
	
	Installed:
	buildapp-1.5.5-2.x86_64
	
	Complete!
	```	
	

