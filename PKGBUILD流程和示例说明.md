## PKGBUILD流程和演示
* 描述了PKGBUILD构建软件包的大致流程和示例演示


### 参考资料
1. [构建软件包指导规则](https://wiki.archlinux.org/title/Arch_package_guidelines_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
2. [PKGBUILD说明文档](https://wiki.archlinux.org/title/PKGBUILD_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
3. [构建软件包指导方法](https://wiki.archlinux.org/title/Creating_packages_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
4. [makepkg指令](https://wiki.archlinux.org/title/Makepkg_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
5. [pacman指令](https://wiki.archlinux.org/title/Pacman_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
6. [AUR](https://aur.archlinux.org/packages)


### 测试环境
* 进行PKGBUILD的编译过程，需要在特定的环境下
1. 操作系统
	
	PKGBUILD只适用于Arch Linux系统
	```
	NAME="Arch Linux"
	PRETTY_NAME="Arch Linux"
	ID=arch
	BUILD_ID=rolling
	ANSI_COLOR="38;2;23;147;209"
	HOME_URL="https://archlinux.org/"
	DOCUMENTATION_URL="https://wiki.archlinux.org/"
	SUPPORT_URL="https://bbs.archlinux.org/"
	BUG_REPORT_URL="https://bugs.archlinux.org/"
	LOGO=archlinux-logo
	```

2. 前置软件包

	进行PKGBUILD包编译时，需要安装的基础依赖软件
	```
	#包含make和其它一些从源码编译时所需要的工具、创建包、检查包的逻辑性
	base-devel
	makepkg
	namcap
	```


### PKGBUILD文件说明
* PKGBUILD是适用于Arch Linux的包编译脚本，其大致和SPEC的流程一致，都是以源码+编译文件为基础，实现源码到二进制包的编译，编译过程都在PKGBUILD文件中描述。PKGBUILD文件主要是分成2部分
1. 变量部分
	* 主要通过变量指明PKGBUILD打包编译过程中的一些重要参数，包括名称、版本、源码、依赖等等

2. 函数部分
	* 主要存在5种函数，用于描述打包编译的全过程，类似于SPEC中的不同阶段的功能

	下面是标准的PKGBUILD文件模板，不同参数及函数的功能写在注释中
	```c
	# 维护者说明
	# Maintainer: Your Name <youremail@domain.com>
	
	#包名
	pkgname=NAME
	
	#版本
	pkgver=VERSION
	
	#发行号
	pkgrel=1
	
	#用于强制升级软件包
	epoch=
	
	#软件包描述
	pkgdesc=""
	
	#支持架构
	arch=()
	
	#待打包软件官方站点的网址
	url=""
	
	#许可
	license=('GPL')
	
	#包组
	groups=()
	
	#软件的生成和运行时必须先行安装的软件列表
	depends=()
	#仅在软件生成时需要的软件包列表
	makedepends=()
	#运行测试组件时需要，而运行时不需要的包列表
	checkdepends=()
	#可选软件包序列
	optdepends=()
	#当前包能提供的功能
	provides=()
	#与当前软件包发生冲突的包与功能的列表
	conflicts=()
	#会因安装当前包而取代的过时的包的列表
	replaces=()
	#当包被升级或卸载时，应当备份的文件（的路径）序列
	backup=()
	
	options=()
	#.install 脚本的名称。pacman 可以在安装、卸载或升级一个软件包时存储及执行一些特定的脚本
	install=
	#软件包的更新日志的文件名
	changelog=
	
	#构建软件包时需要的文件列表
	source=("$pkgname-$pkgver.tar.gz"
			"$pkgname-$pkgver.patch")
	
	#一个在 source 中列出，但不应该在运行 makepkg 时被解包的文件列表
	noextract=()
	
	#source 列表文件中的 128 位 MD5 校验和
	md5sums=()
	
	#PGP 指纹列表
	validpgpkeys=()
	
	#函数阶段 分别是预处理 编译 检查 安装
	prepare() {
			cd "$pkgname-$pkgver"
			patch -p1 -i "$srcdir/$pkgname-$pkgver.patch"
	}
	
	build() {
			cd "$pkgname-$pkgver"
			./configure --prefix=/usr
			make
	}
	
	check() {
			cd "$pkgname-$pkgver"
			make -k check
	}
	
	package() {
			cd "$pkgname-$pkgver"
			make DESTDIR="$pkgdir/" install
	}
	```

* PKGBUILD中有2个关键的环境变量$srcdir和$pkgdir
1. $srcdir
	* 类似于SPEC文件中的BUILD目录，会将源码解压到$srcdir目录下，然后在此目录中完成编译

2. $pkgdir
	* 类似SPEC文件中的BUILDROOT目录，会产生伪root环境，将软件安装到此目录下
	

### 构建流程及结果
* PKGBUILD最重要的就是PKGBUILD文件的编写，其内容是遵守源码包的编译过程，构建过程大致如下：
1. 下载源码包并手动安装测试，记录编译流程和命令

2. 根据手动编译测试，编写PKGBUILD文件

3. makepkg进行编译构建
	```
	makepkg 
	```

4. 安装测试软件包
	```
	pacman -U $pkgname-$pkgver.pkg.tar.gz
	```

5. namcap检查包的逻辑性
	```
	namcap PKGBUILD
	namcap $pkgname-$pkgver.pkg.tar.zst
	```

* 其中3步骤主要借助工具makepkg完成编译构建，其主要完成以下工作：

1. 检查相关依赖是否安装
2. 从指定的服务器下载源文件
3. 解压源文件
4. 编译软件并将它安装于伪root环境下
5. 删除二进制文件和库文件的符号连接
6. 生成包的meta文件
7. 将伪root环境压缩成一个包文件
8. 将生成的包文件保存到配置的文件夹中（默认为当前工作目录）

* PKGBUILD成功编译后得到的二进制文件和可以得到的其他信息如包的版本信息和依赖关系等，都将被打包到一个文件叫$pkgname-$pkgver.pkg.tar.gz里，它包含了以下由 makepkg 生成的文件：

1. 要安装的二进制文件
2. .PKGINFO: 包含所有 pacman 处理软件包的元数据，依赖等等
3. .BUILDINFO: 包含可复现编译需要的信息
4. .MTREE: 包含了文件的哈希值与时间戳. pacman 能够根据这些储存在本地数据库的信息校验软件包的完整性
5. .INSTALL: 可选的文件，可以用来在安装/升级/删除操作之后运行命令(本文件只有在 PKGBUILD 中制定才会存在)
6. .Changelog: 一个可选的文件，保存了包管理员描述软件更新的日志(不是所有包中都存在。)


### 示例演示
* 下面以buildapp的PKGBUILD编译做示例演示

1. 编写buildapp的PKGBUILD文件(此处直接在 [AUR](https://aur.archlinux.org/packages/buildapp) 上下载的)
	```
	git clone https://aur.archlinux.org/buildapp.git
	```
	
	其PKGBUILD的内容如下：
	```c
	# Maintainer:  <aaron.l.france@gmail.com>
	pkgname=buildapp
	pkgver=1.5.5
	pkgrel=2
	epoch=
	pkgdesc="A nice easy way to build Common Lisp images"
	arch=('i686' 'x86_64')
	url=""
	license=('BSD')
	depends=('sbcl')
	makedepends=('sbcl')
	provides=('buildapp')
	source=(https://github.com/xach/$pkgname/archive/release-$pkgver.tar.gz)
	md5sums=('a60d1f7c349075f358a1bf0bf9d1ba9c')
	
	build() {
	cd "$srcdir/$pkgname-release-$pkgver"
	make
	}
	
	package() {
	cd "$srcdir/$pkgname-release-$pkgver"
	mkdir -p "$pkgdir/usr/bin"
	make DESTDIR="$pkgdir/usr" install
	}
	```
2. 执行编译构建，查看编译结果
	```shell
	makepkg
	[shgm@archlinux buildapp]$ ll
	total 10604
	-rw-r--r-- 1 shgm shgm      572 Apr 26 18:17 PKGBUILD
	-rw-r--r-- 1 shgm shgm 10827114 Apr 27 03:14 buildapp-1.5.5-2-x86_64.pkg.tar.zst
	drwxr-xr-x 3 shgm shgm     4096 Apr 27 03:14 pkg
	-rw-r--r-- 1 shgm shgm    16357 Apr 26 11:17 release-1.5.5.tar.gz
	drwxr-xr-x 3 shgm shgm     4096 Apr 27 03:14 src
	```
	
	可以发现存在***src***和***pkg***文件夹，分别对应$srcdir和$pkgdir，而buildapp-1.5.5-2-x86_64.pkg.tar.zst就是最终的二进制包，release-1.5.5.tar.gz是根据source下载的源码包
	
3. 安装测试buildapp
	```
	[shgm@archlinux buildapp]$ sudo pacman -U buildapp-1.5.5-2-x86_64.pkg.tar.zst
	loading packages...
	resolving dependencies...
	looking for conflicting packages...
	
	Packages (1) buildapp-1.5.5-2
	
	Total Installed Size:  39.85 MiB
	
	:: Proceed with installation? [Y/n] y
	(1/1) checking keys in keyring                                                                    [##########################################################] 100%
	(1/1) checking package integrity                                                                  [##########################################################] 100%
	(1/1) loading package files                                                                       [##########################################################] 100%
	(1/1) checking for file conflicts                                                                 [##########################################################] 100%
	(1/1) checking available disk space                                                               [##########################################################] 100%
	:: Processing package changes...
	(1/1) installing buildapp                                                                         [##########################################################] 100%
	:: Running post-transaction hooks...
	(1/1) Arming ConditionNeedsUpdate...
	```

4. namcap检查包的逻辑性
	```
	[shgm@archlinux buildapp]$ namcap PKGBUILD
	PKGBUILD (buildapp) W: Make dependency (sbcl) already included as dependency
	PKGBUILD (buildapp) E: Missing url
	[shgm@archlinux buildapp]$ namcap buildapp-1.5.5-2-x86_64.pkg.tar.zst
	buildapp E: Missing custom license directory (usr/share/licenses/buildapp)
	buildapp W: Dependency included and not needed ('sbcl')
	```
	
	可以发现namcap能够对PKGBUILD和生成的二进制包进行检验，便于快速找到问题
	

### 总结

* PKGBUILD和SPEC的编译打包流程类似，只有使用的工具和文件构成有所不同，后续PKGBUILD转SPEC可以采用变量=>关键词，函数=>阶段的映射转换